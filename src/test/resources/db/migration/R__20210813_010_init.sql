/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

create table type_tab
(
    oid         bigint primary key,
    key         varchar(100),
    description varchar(100)
);

create unique index type_tab_key on type_tab (key);

comment on COLUMN type_tab.oid is 'Internal surrogate id';
comment on COLUMN type_tab.key is 'External identifier';
comment on COLUMN type_tab.description is 'Type description';

create table composite_type_tab
(
    oid         bigint primary key,
    key1        varchar(100),
    key2        varchar(100),
    description varchar(100)
);

alter table composite_type_tab add unique (key1, key2);
create unique index composite_type_tab_key on composite_type_tab (key1, key2);

create table main_tab
(
    oid      bigint primary key,
    col1     varchar(100),
    col2     varchar(100),
    col3     varchar(100),
    type_oid bigint,
    cmp_key1 varchar(100),
    cmp_key2 varchar(100)
);

alter table main_tab
    add constraint main_tab_type_tab foreign key (type_oid) references type_tab (oid);

alter table main_tab
    add constraint main_tab_composite_type_tab foreign key (cmp_key1, cmp_key2) references composite_type_tab (key1, key2);
