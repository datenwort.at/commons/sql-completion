/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import at.datenwort.commons.sqlCompletion.model.DatabaseModel;
import at.datenwort.commons.sqlCompletion.model.DatabaseTableModelProvider;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSqlCompletionWithDatabase extends AbstractTestWithDatabase {

    @Test
    public void suggestTableReference() throws SQLException {

        DatabaseModel databaseModel;
        try (Connection connection = getConnection()) {
            databaseModel = new DatabaseTableModelProvider().createModelFromDatabase(connection);
        }

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "select * from main_tab mt join type_tab tt on");
        sqlCompletionProvider.process();

        List<CompletionItem> completionItemList = sqlCompletionProvider.completeAt(Integer.MAX_VALUE);
        assertThat(completionItemList).anyMatch(item -> "tt.oid = mt.type_oid".equals(item.getInsertText()));
    }

    @Test
    public void suggestTableReferenceRev() throws SQLException {

        DatabaseModel databaseModel;
        try (Connection connection = getConnection()) {
            databaseModel = new DatabaseTableModelProvider().createModelFromDatabase(connection);
        }

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "select * from type_tab tt join main_tab mt on");
        sqlCompletionProvider.process();

        List<CompletionItem> completionItemList = sqlCompletionProvider.completeAt(Integer.MAX_VALUE);
        assertThat(completionItemList).anyMatch(item -> "mt.type_oid = tt.oid".equals(item.getInsertText()));
    }

    @Test
    public void suggestTableReferenceComposite() throws SQLException {

        DatabaseModel databaseModel;
        try (Connection connection = getConnection()) {
            databaseModel = new DatabaseTableModelProvider().createModelFromDatabase(connection);
        }

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "select * from main_tab mt join composite_type_tab ct on");
        sqlCompletionProvider.process();

        List<CompletionItem> completionItemList = sqlCompletionProvider.completeAt(Integer.MAX_VALUE);
        assertThat(completionItemList).anyMatch(item -> "ct.key1 = mt.cmp_key1 and ct.key2 = mt.cmp_key2".equals(item.getInsertText()));
    }

    @Test
    public void suggestTableWithSchema() throws SQLException {

        DatabaseModel databaseModel;
        try (Connection connection = getConnection()) {
            databaseModel = new DatabaseTableModelProvider().createModelFromDatabase(connection);
        }

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "select * from public.");
        sqlCompletionProvider.process();

        List<CompletionItem> completionItemList = sqlCompletionProvider.completeAt(21);
        assertThat(completionItemList).anyMatch(item -> "MAIN_TAB".equals(item.getInsertText()));
    }
}
