/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import org.flywaydb.core.Flyway;
import org.hsqldb.jdbc.JDBCDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class AbstractTestWithDatabase {

    private final static String JDBC_URL = "jdbc:hsqldb:mem:testdb;shutdown=false";
    private final static String JDBC_USER = "sa";

    @BeforeAll
    public static void initDb() {
        try {
            Class.forName(JDBCDriver.class.getName());

            try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
                // start database
                connection.commit();
            }

            // popuplate
            Flyway flyway = new Flyway(Flyway.configure()
                    .dataSource(JDBC_URL, JDBC_USER, null)
            );
            flyway.migrate();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @AfterAll
    public static void shutdow() {
        try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
            statement.execute("SHUTDOWN IMMEDIATELY");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, null);
        return connection;
    }
}
