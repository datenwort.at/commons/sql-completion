/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestTokenizer {

    @Test
    public void endlessLoopTest() {
        String sql = getComplexSql();

        // test sql while it is typed in.
        // mainly to show if there is any endless loop during parsing.
        for (int i = 0; i < sql.length(); i++) {
            SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(null, sql.substring(0, i));
            sqlCompletionProvider.process();
        }
    }

    @SuppressWarnings("JoinAssertThatStatements")
    @Test
    public void tableWithText() {
        Tokens tokenizer = SqlTokenizer.tokenize("select 'abc def''ghi''' from dual");
        assertThat(tokenizer.next()).isEqualTo("select");
        assertThat(tokenizer.next()).isEqualTo("'abcdef''ghi'''");
        assertThat(tokenizer.next()).isEqualTo("from");
        assertThat(tokenizer.next()).isEqualTo("dual");
    }

    protected String getComplexSql() {
        String sql = "WITH\n" +
                "    commission_quantity AS (\n" +
                "        SELECT\n" +
                "            c.oid AS commission_oid,\n" +
                "            SUM(cj.quantity * iqu.quantity) AS full_quantity\n" +
                "        FROM\n" +
                "            wow_commission c\n" +
                "                JOIN wow_comm_jous cj ON cj.commissionoid = c.oid AND cj.quantity > 0.0\n" +
                "                JOIN wow_item_quantity_units iqu ON iqu.oid = cj.itemquantityunitoid\n" +
                "        WHERE\n" +
                "            c.commstatus IN ('STARTED', 'PAUSED', 'READY_FOR_DELIVERY')\n" +
                "        GROUP BY\n" +
                "            c.oid\n" +
                "    ),\n" +
                "    pallet_quantity AS (\n" +
                "        SELECT\n" +
                "            p.commissionoid AS commission_oid,\n" +
                "            SUM(pi.quantity * iqu.quantity) AS full_quantity\n" +
                "        FROM\n" +
                "            wow_pallets p\n" +
                "                JOIN wow_pallet_items pi ON pi.palletoid = p.oid AND pi.quantity > 0.0 AND pi.extraitem = FALSE\n" +
                "                JOIN wow_item_quantity_units iqu ON iqu.oid = pi.itemoid\n" +
                "                JOIN wow_commission c ON c.oid = p.commissionoid\n" +
                "        WHERE\n" +
                "              pi.warehouseoutref = c.commissioningid\n" +
                "          AND pi.warehouseoutreftype = 'WAREHOUSE_OUT_NUMBER'\n" +
                "          AND c.commstatus IN ('STARTED', 'PAUSED', 'READY_FOR_DELIVERY')\n" +
                "        GROUP BY p.commissionoid\n" +
                "    )\n" +
                "SELECT\n" +
                "    cq.commission_oid AS commissionoid,\n" +
                "    CASE\n" +
                "        WHEN pq.full_quantity >= cq.full_quantity THEN 100\n" +
                "        WHEN pq.full_quantity IS NULL OR pq.full_quantity = 0 THEN 0\n" +
                "        ELSE (pq.full_quantity * 100.0) / cq.full_quantity\n" +
                "        END AS progress\n" +
                "FROM\n" +
                "    commission_quantity cq\n" +
                "        LEFT JOIN pallet_quantity pq ON pq.commission_oid = cq.commission_oid\n" +
                "WHERE\n" +
                "        cq.commission_oid\n" +
                "--p:commissions: = ANY(?)\n" +
                "        IN (SELECT DISTINCT commissionoid FROM wow_comm_jous WHERE orderid = 'O0383091')\n" +
                "--p\n";
        return sql;
    }
}
