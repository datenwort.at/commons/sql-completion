/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import at.datenwort.commons.sqlCompletion.model.DatabaseModel;
import at.datenwort.commons.sqlCompletion.model.SimpleDatabaseModel;
import at.datenwort.commons.sqlCompletion.model.TableModel;
import at.datenwort.commons.sqlCompletion.namingStrategy.DefaultSchemaNamingStrategy;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSqlCompletionProvider {
    @Test
    public void complexSqlTest() {
        DatabaseModel databaseModel = buildDatabaseModel();

        String sql = getComplexSql();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.getColumns("commission_quantity")).hasSize(2);
        assertThat(sqlCompletionProvider.getColumns("cq")).hasSize(2);
        assertThat(sqlCompletionProvider.getColumns("pallet_quantity")).hasSize(2);
        assertThat(sqlCompletionProvider.getColumns("pq")).hasSize(2);
        assertThat(sqlCompletionProvider.hasAlias("cj")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("iqu")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("pi")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("c")).isTrue();
    }

    @Test
    public void statementWithErrorTest() {
        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "SELECT * FROM wow_pallets t1 JOIN wow_pallet_items as t2 ON1 t2.a = LEFT JOIN wow_commission t3 ON t3.a = t1.b");
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.hasAlias("t1")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t2")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t3")).isTrue();
    }

    @Test
    public void defaultSchemaNamingTest() {
        DatabaseModel databaseModel = buildDatabaseModel();

        String sql = "SELECT * FROM wow_pallets t1 JOIN wow_pallet_items as t2 ON t2.a = LEFT JOIN wow_commission t3 ON t3.a = t1.b";
        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.hasAlias("t1")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t2")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t3")).isTrue();

        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "wow_pallet_items as t2 ON t2."));
        assertThat(completionItems).hasSize(1)
                .anyMatch(item -> "c5".equals(item.getInsertText()));

        completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "SELECT * FROM "));
        assertThat(completionItems).hasSize(5)
                .anyMatch(item -> "wow_comm_jous".equals(item.getInsertText()));

    }

    @Test
    public void tableWithSchemaTest() {
        List<TableModel> databaseModel = new ArrayList<>();
        databaseModel.add(new TableModel(null, "xyz", "tabl1", null).addColumn("c1", null));
        databaseModel.add(new TableModel(null, "abc", "tabl2", null).addColumn("c2", null));

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(createDatabaseModel(databaseModel),
                "SELECT * FROM xyz.tabl1 t1 JOIN abc.tabl2 as t2 ON t2.a = t1.a JOIN xyz.tabl3 t3 on t3.a = t1.a");
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.hasAlias("t1")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t2")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t3")).isFalse();
        assertThat(sqlCompletionProvider.getColumns("t1")).anyMatch(columnModel -> "c1".equals(columnModel.getName()));
        assertThat(sqlCompletionProvider.getColumns("t2")).anyMatch(columnModel -> "c2".equals(columnModel.getName()));

        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(20);
        assertThat(completionItems).anyMatch(ci -> "tabl1".equalsIgnoreCase(ci.getInsertText()));
    }

    @Test
    public void tableWithText() {
        List<TableModel> databaseModel = new ArrayList<>();
        databaseModel.add(new TableModel(null, "xyz", "tabl1", null).addColumn("c1", null));
        databaseModel.add(new TableModel(null, "abc", "tabl2", null).addColumn("c2", null));

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(createDatabaseModel(databaseModel),
                "select 'abc def''' from dual");
        sqlCompletionProvider.process();
    }

    @Test
    public void completionTestJoinOn() {
        String sql = getComplexSql();

        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        // should find the list of columns for table wow_comm_jous
        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "JOIN wow_comm_jous cj ON cj.commissionoid"));
        assertThat(completionItems).anyMatch(item -> "c2".equals(item.getInsertText()));
    }

    @Test
    public void completionTestJoin() {
        String sql = getComplexSql();

        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        // should find a list of all tables
        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "JOIN "));
        assertThat(completionItems).hasSize(7)
                .anyMatch(item -> "wow_commission".equals(item.getInsertText()))
                .anyMatch(item -> "commission_quantity".equals(item.getInsertText()));
    }

    @Test
    public void completionTestJoinPartTableName() {
        String sql = getComplexSql();

        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        // should find a list of all tables
        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "JOIN wow_"));
        assertThat(completionItems).hasSize(7)
                .anyMatch(item -> "wow_commission".equals(item.getInsertText()))
                .anyMatch(item -> "commission_quantity".equals(item.getInsertText()));
    }

    @Test
    public void completionTestJoinPartTableNameWithSchmea() {
        String sql = getComplexSql();

        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        // should find a list of all tables
        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "JOIN public.wow_"));
        assertThat(completionItems).hasSize(7)
                .anyMatch(item -> "wow_commission".equals(item.getInsertText()))
                .anyMatch(item -> "commission_quantity".equals(item.getInsertText()));
    }

    @Test
    public void completionTestFrom() {
        String sql = getComplexSql();

        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel, sql);
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        // should find a list of all tables
        List<CompletionItem> completionItems = sqlCompletionProvider.completeAt(Utils.endIndexOf(sql, "FROM "));
        assertThat(completionItems).hasSize(7)
                .anyMatch(item -> "wow_commission".equals(item.getInsertText()))
                .anyMatch(item -> "commission_quantity".equals(item.getInsertText()));
    }

    @Test
    public void multilineStatementWithCommentTest_LUNIX() {
        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "SELECT * FROM wow_pallets t1 \n" +
                        "-- this is some sql comment\n" +
                        "JOIN wow_pallet_items as t2 ON t2.a = LEFT JOIN wow_commission t3 ON t3.a = t1.b");
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.hasAlias("t1")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t2")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t3")).isTrue();
    }

    @Test
    public void multilineStatementWithCommentTest_DOS() {
        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "SELECT * FROM wow_pallets t1 \r\n" +
                        "-- this is some sql comment\r\n" +
                        "JOIN wow_pallet_items as t2 ON t2.a = LEFT JOIN wow_commission t3 ON t3.a = t1.b");
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.hasAlias("t1")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t2")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t3")).isTrue();
    }

    @Test
    public void multilineStatementWithCommentTest_MAC() {
        DatabaseModel databaseModel = buildDatabaseModel();

        SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseModel,
                "SELECT * FROM wow_pallets t1 \r" +
                        "-- this is some sql comment\r" +
                        "JOIN wow_pallet_items as t2 ON t2.a = LEFT JOIN wow_commission t3 ON t3.a = t1.b");
        sqlCompletionProvider.setNamingStrategy(new DefaultSchemaNamingStrategy("public"));
        sqlCompletionProvider.process();

        assertThat(sqlCompletionProvider.hasAlias("t1")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t2")).isTrue();
        assertThat(sqlCompletionProvider.hasAlias("t3")).isTrue();
    }

    private DatabaseModel createDatabaseModel(List<TableModel> databaseModel) {
        SimpleDatabaseModel ret = new SimpleDatabaseModel();
        ret.setupTables(databaseModel.stream().collect(Collectors.toMap(TableModel::getKey, Function.identity())));
        return ret;
    }

    protected DatabaseModel buildDatabaseModel() {
        List<TableModel> databaseModel = new ArrayList<>();
        databaseModel.add(new TableModel(null, "public", "wow_commission", null).addColumn("c1", null));
        databaseModel.add(new TableModel(null, "public", "wow_comm_jous", null).addColumn("c2", null));
        databaseModel.add(new TableModel(null, "public", "wow_item_quantity_units", null).addColumn("c3", null));
        databaseModel.add(new TableModel(null, "public", "wow_pallets", null).addColumn("c4", null));
        databaseModel.add(new TableModel(null, "public", "wow_pallet_items", null).addColumn("c5", null));
        return createDatabaseModel(databaseModel);
    }

    protected String getComplexSql() {
        String sql = "WITH\n" +
                "    commission_quantity AS (\n" +
                "        SELECT\n" +
                "            c.oid AS commission_oid,\n" +
                "            SUM(cj.quantity * iqu.quantity) AS full_quantity\n" +
                "        FROM\n" +
                "            wow_commission c\n" +
                "                JOIN wow_comm_jous cj ON cj.commissionoid = c.oid AND cj.quantity > 0.0\n" +
                "                JOIN wow_item_quantity_units iqu ON iqu.oid = cj.itemquantityunitoid\n" +
                "        WHERE\n" +
                "            c.commstatus IN ('STARTED', 'PAUSED', 'READY_FOR_DELIVERY')\n" +
                "        GROUP BY\n" +
                "            c.oid\n" +
                "    ),\n" +
                "    pallet_quantity AS (\n" +
                "        SELECT\n" +
                "            p.commissionoid AS commission_oid,\n" +
                "            SUM(pi.quantity * iqu.quantity) AS full_quantity\n" +
                "        FROM\n" +
                "            wow_pallets p\n" +
                "                JOIN public.wow_pallet_items pi ON pi.palletoid = p.oid AND pi.quantity > 0.0 AND pi.extraitem = FALSE\n" +
                "                JOIN wow_item_quantity_units iqu ON iqu.oid = pi.itemoid\n" +
                "                JOIN wow_commission c ON c.oid = p.commissionoid\n" +
                "        WHERE\n" +
                "              pi.warehouseoutref = c.commissioningid\n" +
                "          AND pi.warehouseoutreftype = 'WAREHOUSE_OUT_NUMBER'\n" +
                "          AND c.commstatus IN ('STARTED', 'PAUSED', 'READY_FOR_DELIVERY')\n" +
                "        GROUP BY p.commissionoid\n" +
                "    )\n" +
                "SELECT\n" +
                "    cq.commission_oid AS commissionoid,\n" +
                "    CASE\n" +
                "        WHEN pq.full_quantity >= cq.full_quantity THEN 100\n" +
                "        WHEN pq.full_quantity IS NULL OR pq.full_quantity = 0 THEN 0\n" +
                "        ELSE (pq.full_quantity * 100.0) / cq.full_quantity\n" +
                "        END AS progress\n" +
                "FROM\n" +
                "    commission_quantity cq\n" +
                "        LEFT JOIN pallet_quantity pq ON pq.commission_oid = cq.commission_oid\n" +
                "WHERE\n" +
                "        cq.commission_oid\n" +
                "--p:commissions: = ANY(?)\n" +
                "        IN (SELECT DISTINCT commissionoid FROM wow_comm_jous WHERE orderid = 'O0383091')\n" +
                "--p\n";
        return sql;
    }
}
