/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings({"UnusedReturnValue", "UnnecessaryLocalVariable", "unused"})
public class Tokens {
    private final List<String> tokens;
    private int pos = -1;

    Tokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public String next() {
        if (pos + 1 >= tokens.size()) {
            return null;
        }

        String ret = null;
        do {
            pos++;
            if (pos >= tokens.size()) {
                break;
            }

            ret = tokens.get(pos);
        }
        while (ret.trim().isEmpty());

        return ret;
    }

    public String anyNext() {
        if (pos + 1 >= tokens.size()) {
            return null;
        }

        pos++;
        String ret = tokens.get(pos);

        return ret;
    }

    public String prev() {
        if (pos <= 0) {
            return null;
        }

        String ret;
        do {
            if (pos <= 0) {
                return null;
            }

            pos--;
            ret = tokens.get(pos);
        }
        while (ret.trim().isEmpty());

        return ret;
    }

    public String peekPrev() {
        if (pos <= 0) {
            return null;
        }

        int offset = 0;
        String ret = null;
        do {
            offset++;
            if (pos - offset <= 0) {
                break;
            }

            ret = tokens.get(pos - offset);
        }
        while (ret.trim().isEmpty());
        return ret;
    }

    public String peekNext() {
        if (pos >= tokens.size()) {
            return null;
        }

        int offset = 0;
        String ret = null;
        do {
            offset++;
            if (offset + pos >= tokens.size()) {
                break;
            }

            ret = tokens.get(pos + offset);
        }
        while (ret.trim().isEmpty());

        return ret;
    }

    public String peekNext(int offset) {
        if (pos + offset >= tokens.size()) {
            return null;
        }

        String ret = null;
        do {
            offset++;
            if (offset + pos >= tokens.size()) {
                break;
            }

            ret = tokens.get(pos + offset);
        }
        while (ret.trim().isEmpty());

        return ret;
    }

    public boolean ifNext(String s) {
        String next = peekNext();
        //noinspection RedundantIfStatement
        if (s.equals(next)) {
            return true;
        }
        return false;
    }

    public boolean ifPrev(String s) {
        String next = peekPrev();
        //noinspection RedundantIfStatement
        if (s.equals(next)) {
            return true;
        }
        return false;
    }

    public Tokens subBetween(String startToken, String endToken) {
        List<String> subTokens = new ArrayList<>();

        int level = 0;
        String token;
        while ((token = anyNext()) != null) {
            if (level > 0) {
                subTokens.add(token);
            }

            if (startToken.equals(token)) {
                level++;
                continue;
            }

            if (endToken.equals(token)) {
                level--;
                if (level <= 0) {
                    break;
                }
            }
        }
        return new Tokens(subTokens);
    }

    public Tokens subUntil(Set<String> anyOf, Set<String> pushback) {
        List<String> subTokens = null;

        int level = 0;
        String token;
        while ((token = anyNext()) != null) {
            if ("\n".equals(token)) {
                continue;
            }

            if (level <= 0 && anyOf.contains(token)) {
                if (pushback.contains(token)) {
                    prev();
                }
                break;
            }

            if (subTokens == null) {
                subTokens = new ArrayList<>();
            }
            subTokens.add(token);

            if ("(".equals(token)) {
                level++;
                continue;
            }

            if (")".equals(token)) {
                level--;
                if (level < 0) {
                    break;
                }
            }
        }

        return subTokens != null ? new Tokens(subTokens) : null;
    }

    public void drain(Consumer<String> tokenConsumer) {
        String token;
        while ((token = anyNext()) != null) {
            tokenConsumer.accept(token);
        }
    }

    @Override
    public String toString() {
        String ret = tokens.toString();
        return ret;
    }

    public String last() {
        return tokens.get(tokens.size() - 1);
    }

    public void skipUntil(Set<String> anyOf) {
        String token;
        while ((token = anyNext()) != null) {
            if (anyOf.contains(token)) {
                break;
            }
        }
    }

    public boolean ifMatch(String... patterns) {
        int memoryPos = pos;
        try {
            for (String pattern : patterns) {
                String token = next();
                if (!Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(token).matches()) {
                    return false;
                }
            }

            return true;
        } finally {
            pos = memoryPos;
        }
    }

    public String peekFindToken(Set<String> anyOf) {
        if (pos >= tokens.size()) {
            return null;
        }

        int offset = 0;
        do {
            offset++;
            if (offset + pos >= tokens.size()) {
                return null;
            }

            String token = tokens.get(pos + offset);
            if (token != null && anyOf.contains(token)) {
                return token;
            }
        }
        while (true);
    }

    public Optional<Integer> indexOf(String searchToken) {
        int offset = -1;
        do {
            offset++;
            if (offset + pos >= tokens.size()) {
                return Optional.empty();
            }

            String token = tokens.get(pos + offset);
            if (searchToken.equals(token)) {
                return Optional.of(offset);
            }
        }
        while (true);
    }
}
