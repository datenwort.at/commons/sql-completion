/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import at.datenwort.commons.sqlCompletion.model.ColumnModel;
import at.datenwort.commons.sqlCompletion.model.DatabaseModel;
import at.datenwort.commons.sqlCompletion.model.TableModel;
import at.datenwort.commons.sqlCompletion.namingStrategy.DefaultNamingStrategy;
import at.datenwort.commons.sqlCompletion.namingStrategy.NamingStrategy;
import org.jetbrains.annotations.Contract;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"UnnecessaryLocalVariable"})
public class SqlCompletionProvider {

    private final String sql;
    private final DatabaseModel databaseModel;
    private Map<String, TableModel> tables;
    private Map<String, TableModel> aliases;

    private CompletionItemBuilder completionItemBuilder;
    private NamingStrategy namingStrategy;

    public SqlCompletionProvider(DatabaseModel databaseModel, String sql) {
        this.databaseModel = databaseModel;
        this.sql = sql;
    }

    @Contract("null -> false")
    public boolean hasAlias(String tableAlias) {
        boolean ret = aliases != null && aliases.containsKey(tableAlias);
        return ret;
    }

    public Collection<ColumnModel> getColumns(String tableAlias) {
        TableModel tableModel = aliases != null ? aliases.get(tableAlias) : null;
        if (tableModel == null) {
            tableModel = tables != null ? tables.get(tableAlias) : null;
        }
        if (tableModel == null) {
            tableModel = databaseModel != null ? namingStrategy.lookupTable(databaseModel, tableAlias) : null;
        }
        if (tableModel == null) {
            return null;
        }

        return tableModel.getColumns();
    }

    @SuppressWarnings("unused")
    public CompletionItemBuilder getCreationItemBuilder() {
        return completionItemBuilder;
    }

    @SuppressWarnings("unused")
    public void setCreationItemBuilder(CompletionItemBuilder completionItemBuilder) {
        this.completionItemBuilder = completionItemBuilder;
    }

    @SuppressWarnings("unused")
    public NamingStrategy getNamingStrategy() {
        return namingStrategy;
    }

    @SuppressWarnings("unused")
    public void setNamingStrategy(NamingStrategy namingStrategy) {
        this.namingStrategy = namingStrategy;
    }

    public void process() {
        tables = new LinkedHashMap<>();
        aliases = new LinkedHashMap<>();
        if (completionItemBuilder == null) {
            completionItemBuilder = new CompletionItemBuilder();
        }

        if (namingStrategy == null) {
            namingStrategy = new DefaultNamingStrategy();
        }

        Tokens sqlTokens = SqlTokenizer.tokenize(sql);
        String sqlToken;
        Boolean withStatement = null;

        while ((sqlToken = sqlTokens.next()) != null) {
            if (sqlToken.startsWith("--")) {
                sqlTokens.skipUntil(Utils.setOf("\n", "\r"));
                continue;
            }

            if (withStatement == null && "with".equals(sqlToken)) {
                withStatement = Boolean.TRUE;
                continue;
            }
            if (Boolean.TRUE.equals(withStatement)) {
                if ("as".equals(sqlToken)) {
                    String withViewName = sqlTokens.peekPrev();
                    if (withViewName == null) {
                        continue;
                    }

                    if (sqlTokens.ifNext("(")) {
                        Tokens withStatementTokens = sqlTokens.subBetween("(", ")");
                        Collection<String> columnNames = parseColumnNames(withStatementTokens);

                        TableModel tableModel = new TableModel(null, null, withViewName, null);
                        if (!tables.containsKey(tableModel.getFullName())) {
                            tables.put(tableModel.getFullName(), tableModel);
                            columnNames.forEach(columnName -> tableModel.addColumn(columnName, null));
                        }

                        withStatementTokens.drain(withToken -> parseTableReference(withStatementTokens, withToken));

                        if (!sqlTokens.ifNext(",")) {
                            withStatement = Boolean.FALSE;
                        }
                    }
                }
            } else {
                parseTableReference(sqlTokens, sqlToken);
            }
        }
    }

    protected Collection<String> parseColumnNames(Tokens withStatementTokens) {
        Set<String> ret = new LinkedHashSet<>();

        int selectStatement = 0;
        String sqlToken;
        while ((sqlToken = withStatementTokens.next()) != null) {
            if ("select".equals(sqlToken)) {
                selectStatement++;
                continue;
            }

            if ("from".equals(sqlToken)) {
                selectStatement--;
                if (selectStatement <= 0) {
                    break;
                }
            }

            if (selectStatement > 0) {
                Tokens columnTokens;
                withStatementTokens.prev();
                while ((columnTokens = withStatementTokens.subUntil(Utils.setOf(",", "from"), Utils.setOf("from"))) != null) {
                    ret.add(columnTokens.last());
                }
            }
        }

        return ret;
    }

    protected void parseTableReference(Tokens sqlTokens, String sqlToken) {
        if (sqlTokens.ifPrev("join") || sqlTokens.ifPrev("from")) {
            //noinspection UnnecessaryLocalVariable
            String tableName = sqlToken;

            if (sqlTokens.ifNext("as")) {
                sqlTokens.next();
            }
            String alias = sqlTokens.next();

            if (alias != null) {
                TableModel tableModel = tables.get(tableName);
                if (tableModel == null && databaseModel != null) {
                    tableModel = namingStrategy.lookupTable(databaseModel, tableName);
                }
                if (tableModel != null) {
                    this.aliases.put(alias, tableModel);
                }
            }
        }
    }

    public List<CompletionItem> completeAt(int position) {
        if (position < 0) {
            return Collections.emptyList();
        }

        Tokens sqlTokens = SqlTokenizer.tokenizeReverse(Utils.left(sql, position));

        String nearestToken = sqlTokens.peekFindToken(Utils.setOf("join", "from", "on", "where", "select"));

        String token;
        if ((token = sqlTokens.next()) != null) {
            for (String subToken : token.split("\\.", 2)) {
                if (subToken.isEmpty()) {
                    continue;
                }

                Collection<ColumnModel> columns = getColumns(subToken);
                if (columns != null) {
                    List<CompletionItem> ret = columns.stream()
                            .map(column -> completionItemBuilder.createForColumn(namingStrategy, column))
                            .collect(Collectors.toList());
                    return ret;
                }
            }

            if ("join".equals(nearestToken) || "from".equals(nearestToken)) {
                String schema = getIfSchema(token);

                //noinspection DuplicatedCode
                List<CompletionItem> ret =
                        Stream.concat(
                                        tables.values().stream()
                                                .filter(table -> !Utils.isEmpty(table.getColumns()))
                                                .map(table1 -> completionItemBuilder.createForTable(namingStrategy, null, table1)),
                                        databaseModel.getTables().stream()
                                                .filter(table ->
                                                        (schema == null || schema.equalsIgnoreCase(table.getSchema()))
                                                                && !Utils.isEmpty(table.getColumns()))
                                                .map(table2 -> completionItemBuilder.createForTable(namingStrategy, schema, table2))
                                )
                                .collect(Collectors.toList());
                return ret;
            }

            if ("on".equals(nearestToken)) {
                String mainTableAlias = sqlTokens.indexOf("on")
                        .map(sqlTokens::peekNext)
                        .orElse(null);
                if (hasAlias(mainTableAlias)) {
                    TableModel mainTableModel = aliases.get(mainTableAlias);
                    List<Map.Entry<String, TableModel>> otherTables = aliases.entrySet().stream()
                            .filter(alias -> !mainTableAlias.equals(alias.getKey()))
                            .collect(Collectors.toList());

                    List<CompletionItem> ret = Stream.concat(
                                    otherTables.stream()
                                            .flatMap(otherTable -> databaseModel.lookupReferences(mainTableModel, otherTable.getValue()).stream()
                                                    .map(reference -> completionItemBuilder.createForReference(namingStrategy, mainTableModel, reference, mainTableAlias, otherTable.getKey()))),

                                    aliases.entrySet().stream()
                                            .map(entry -> completionItemBuilder.createForAlias(namingStrategy, entry.getKey(), entry.getValue())))
                            .collect(Collectors.toList());
                    return ret;
                }
            }

            if ("where".equals(nearestToken)) {
                List<CompletionItem> ret =
                        aliases.entrySet().stream()
                                .map(entry -> completionItemBuilder.createForAlias(namingStrategy, entry.getKey(), entry.getValue()))
                                .collect(Collectors.toList());
                return ret;
            }
        }

        return Collections.emptyList();
    }

    private String getIfSchema(String token) {
        if (token == null) {
            return null;
        }

        int schemaPos = token.lastIndexOf(".");
        if (schemaPos > 0) {
            token = token.substring(0, schemaPos).trim();
        }

        if (token.isEmpty()) {
            return null;
        }

        String finalToken = token;
        String schemaName = databaseModel.getTables().stream()
                .map(TableModel::getSchema)
                .filter(schema -> schema.equalsIgnoreCase(finalToken))
                .findFirst()
                .orElse(null);
        return schemaName;
    }
}
