/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

public class CompletionItem {
    private String label;
    private String detail;
    private String insertText;
    private String documentation;

    public CompletionItem() {
    }

    @SuppressWarnings("unused")
    public CompletionItem(String label, String detail, String insertText, String documentation) {
        this.label = label;
        this.detail = detail;
        this.insertText = insertText;
        this.documentation = documentation;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CompletionItem{");
        sb.append("insertText='").append(insertText).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getInsertText() {
        return insertText;
    }

    public void setInsertText(String insertText) {
        this.insertText = insertText;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }
}
