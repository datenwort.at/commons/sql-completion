/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public final class Utils {
    private Utils() {
    }

    public static String lower(String text) {
        if (text == null) {
            return null;
        }

        return text.toLowerCase(Locale.ROOT);
    }

    public static Set<String> setOf(String... elements) {
        if (elements == null) {
            return Collections.emptySet();
        }

        return new LinkedHashSet<>(Arrays.asList(elements));
    }

    public static String left(String text, int count) {
        if (text == null) {
            return null;
        }

        if (text.length() < count) {
            return text;
        }

        return text.substring(0, count);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static int endIndexOf(String text, String search) {
        int pos = text.indexOf(search);
        if (pos < 0) {
            return pos;
        }

        return pos + search.length() - 1;
    }

    public static String from(String text, int index) {
        if (text == null) {
            return null;
        }
        if (text.length() > index) {
            return text.substring(index);
        }
        return null;
    }

    public static <VALUE> VALUE value(Map.Entry<?, VALUE> entry) {
        if (entry == null) {
            return null;
        }

        return entry.getValue();
    }

    public static int compare(String t1, String t2) {
        //noinspection StringEquality
        if (t1 == t2) {
            return 0;
        }

        if (t1 == null) {
            return t2 != null ? -1 : 0;
        }
        if (t2 == null) {
            return 1;
        }

        return t1.compareTo(t2);
    }

    public static <T> T nvl(T... elements) {
        if (elements == null || elements.length < 1) {
            return null;
        }

        for (T element : elements) {
            if (element != null) {
                return element;
            }
        }

        return null;
    }
}
