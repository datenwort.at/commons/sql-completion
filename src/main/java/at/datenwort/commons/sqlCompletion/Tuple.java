/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

public class Tuple<TYPE> {
    private final TYPE t1;
    private final TYPE t2;

    public Tuple(TYPE t1, TYPE t2) {
        this.t1 = t1;
        this.t2 = t2;
    }

    public TYPE getT1() {
        return t1;
    }

    public TYPE getT2() {
        return t2;
    }

    public Tuple<TYPE> swap() {
        return new Tuple<>(t2, t1);
    }
}
