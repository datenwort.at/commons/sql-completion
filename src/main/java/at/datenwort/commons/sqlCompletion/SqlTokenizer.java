/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

@SuppressWarnings("UnnecessaryLocalVariable")
public final class SqlTokenizer {
    private SqlTokenizer() {
    }

    public static Tokens tokenize(String sql) {
        Tokens sqlTokens = new Tokens(split(sql));
        return sqlTokens;
    }

    public static Tokens tokenizeReverse(String sql) {
        List<String> tokens = split(sql);
        Collections.reverse(tokens);

        Tokens sqlTokens = new Tokens(tokens);

        return sqlTokens;
    }

    private static List<String> split(String sql) {
        List<String> ret = Arrays.asList(sql.toLowerCase(Locale.ROOT).split(
                "(" +
                        "(?=[\\n\\r]+|(?<=[\\n\\r]))" +
                        "|" +
                        "[\\s]+" +
                        "|" +
                        "(?=[(),]|(?<=[(),]))" +
                        ")"
        ));
        ret = rejoinText(ret);
        return ret;
    }

    private static List<String> rejoinText(List<String> tokens) {
        Pattern newLineOnly = Pattern.compile("^[\\n\\r]+$");

        List<String> ret = new ArrayList<>(tokens.size());

        StringBuilder sb = new StringBuilder(1024);

        String surrogateToken = null;
        for (String token : tokens) {
            if (newLineOnly.matcher(token).matches()) {
                token = "\n";
            } else {
                token = token.trim();
                if (token.length() < 1) {
                    continue;
                }
            }

            if (surrogateToken == null) {
                if (token.startsWith("\"")) {
                    surrogateToken = "\"";
                }
                if (token.startsWith("'")) {
                    surrogateToken = "'";
                }
            }

            if (surrogateToken != null) {
                sb.append(token);

                if (token.endsWith(surrogateToken)) {
                    ret.add(sb.toString());
                    sb.setLength(0);

                    surrogateToken = null;
                }
            } else {
                ret.add(token);
            }
        }

        if (sb.length() > 0) {
            ret.add(sb.toString());
        }

        return ret;
    }
}
