/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion;

import at.datenwort.commons.sqlCompletion.model.ColumnModel;
import at.datenwort.commons.sqlCompletion.model.ReferenceModel;
import at.datenwort.commons.sqlCompletion.model.TableModel;
import at.datenwort.commons.sqlCompletion.namingStrategy.NamingStrategy;

public class CompletionItemBuilder {

    public CompletionItem createForAlias(NamingStrategy namingStrategy, String alias, TableModel referencedTable) {
        CompletionItem completionItem = new CompletionItem();
        completionItem.setLabel(alias);
        completionItem.setInsertText(alias);
        completionItem.setDetail(referencedTable.getName());
        return completionItem;
    }

    public CompletionItem createForColumn(NamingStrategy namingStrategy, ColumnModel column) {
        CompletionItem completionItem = new CompletionItem();
        completionItem.setLabel(column.getName());
        completionItem.setInsertText(column.getName());
        completionItem.setDetail(column.getTableModel().getFullName());
        completionItem.setDocumentation(column.getDocumentation());
        return completionItem;
    }

    public CompletionItem createForTable(NamingStrategy namingStrategy, String filteredSchema, TableModel table) {
        CompletionItem completionItem = new CompletionItem();
        completionItem.setLabel(table.getName());
        completionItem.setInsertText(namingStrategy.toInsertText(filteredSchema, table));
        completionItem.setDetail(table.getFullName());
        completionItem.setDocumentation(table.getDocumentation());
        return completionItem;
    }

    public CompletionItem createForReference(NamingStrategy namingStrategy, TableModel mainTableModel, ReferenceModel reference, String mainTableAlias, String refTableAlias) {

        StringBuilder sb = new StringBuilder();
        for (Tuple<ColumnModel> column : reference.getColumns(mainTableModel)) {
            if (sb.length() > 0) {
                sb.append(" and ");
            }
            sb.append(mainTableAlias);
            sb.append(".");
            sb.append(column.getT1().getName());
            sb.append(" = ");
            sb.append(refTableAlias);
            sb.append(".");
            sb.append(column.getT2().getName());
        }
        String join = sb.toString();

        CompletionItem completionItem = new CompletionItem();
        completionItem.setLabel(join);
        completionItem.setInsertText(join);
        completionItem.setDetail(reference.getFkTable().getName() + " <=> " + reference.getPkTable().getName());
        completionItem.setDocumentation(null);
        return completionItem;
    }
}
