/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.namingStrategy;

import at.datenwort.commons.sqlCompletion.Utils;
import at.datenwort.commons.sqlCompletion.model.DatabaseModel;
import at.datenwort.commons.sqlCompletion.model.TableModel;

public class DefaultNamingStrategy implements NamingStrategy {
    @Override
    public TableModel lookupTable(DatabaseModel databaseModel, String tableName) {
        String lookupSchema;
        final String lookupTableName;
        int schemaSeparator = tableName.indexOf('.');
        if (schemaSeparator < 0) {
            lookupSchema = null;
            lookupTableName = tableName;
        } else {
            lookupSchema = Utils.left(tableName, schemaSeparator);
            lookupTableName = Utils.from(tableName, schemaSeparator + 1);
        }

        return databaseModel.lookupTable(new TableModel.Key(null, lookupSchema, lookupTableName));
    }

    @Override
    public String toInsertText(String filteredSchema, TableModel table) {
        String ret = table.getFullName();
        if (filteredSchema != null && ret.startsWith(filteredSchema + ".")) {
            return ret.substring(filteredSchema.length() + 1);
        }
        return ret;
    }
}
