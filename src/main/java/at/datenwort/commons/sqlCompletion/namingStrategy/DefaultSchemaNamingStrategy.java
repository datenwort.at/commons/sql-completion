/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.namingStrategy;

import at.datenwort.commons.sqlCompletion.Utils;
import at.datenwort.commons.sqlCompletion.model.DatabaseModel;
import at.datenwort.commons.sqlCompletion.model.TableModel;

@SuppressWarnings({"unused", "UnnecessaryLocalVariable"})
public class DefaultSchemaNamingStrategy implements NamingStrategy {
    private final String defaultSchema;

    public DefaultSchemaNamingStrategy(String defaultSchema) {
        this.defaultSchema = defaultSchema;
    }


    @Override
    public TableModel lookupTable(DatabaseModel databaseModel, String tableName) {

        String lookupSchema;
        final String lookupTableName;
        int schemaSeparator = tableName.indexOf('.');
        if (schemaSeparator < 0) {
            lookupSchema = defaultSchema;
            lookupTableName = tableName;
        } else {
            lookupSchema = Utils.left(tableName, schemaSeparator);
            lookupTableName = Utils.from(tableName, schemaSeparator + 1);
        }

        TableModel ret = databaseModel.lookupTable(new TableModel.Key(null, lookupSchema, lookupTableName));
        return ret;
    }

    @Override
    public String toInsertText(String filteredSchema, TableModel table) {
        String fullName = table.getFullName();

        final String ret;
        if (fullName.startsWith(defaultSchema + ".")) {
            ret = fullName.substring(defaultSchema.length() + 1);
        } else {
            ret = fullName;
        }

        return ret;
    }
}
