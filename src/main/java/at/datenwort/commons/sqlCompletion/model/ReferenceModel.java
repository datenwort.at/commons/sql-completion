/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.model;

import at.datenwort.commons.sqlCompletion.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReferenceModel {
    private final TableModel pkTable;
    private final TableModel fkTable;

    private final List<Tuple<ColumnModel>> columns = new ArrayList<>();

    public ReferenceModel(TableModel pkTable, TableModel fkTable) {
        this.pkTable = pkTable;
        this.fkTable = fkTable;
    }

    public TableModel getPkTable() {
        return pkTable;
    }

    public TableModel getFkTable() {
        return fkTable;
    }

    public void add(ColumnModel pkColumn, ColumnModel fkColumn) {
        this.columns.add(new Tuple<>(pkColumn, fkColumn));
    }

    public boolean matches(TableModel mainTable, TableModel otherTable) {
        boolean ret = (pkTable == mainTable && fkTable == otherTable)
                || (pkTable == otherTable && fkTable == mainTable);
        return ret;
    }

    public List<Tuple<ColumnModel>> getColumns(TableModel tableModelOnLeft) {
        List<Tuple<ColumnModel>> ret = columns.stream()
                .map(tuple -> {
                    if (tuple.getT1().getTableModel() == tableModelOnLeft) {
                        return tuple;
                    } else {
                        return tuple.swap();
                    }
                }).collect(Collectors.toList());
        return ret;
    }
}
