/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.model;

import at.datenwort.commons.sqlCompletion.Utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class SimpleDatabaseModel implements DatabaseModel {
    private Map<TableModel.Key, TableModel> tables;
    private TreeMap<TableModel.Key, TableModel> tablesNavigatable;
    private Collection<ReferenceModel> references;

    public void setupTables(Map<TableModel.Key, TableModel> tables) {
        this.tables = tables != null ? Collections.unmodifiableMap(tables) : Collections.emptyMap();
        this.tablesNavigatable = new TreeMap<>(this.tables);
    }

    public void setupReferences(Collection<ReferenceModel> references) {
        this.references = references;
    }

    public Collection<ReferenceModel> lookupReferences(TableModel mainTable, TableModel otherTable) {
        if (mainTable == null || otherTable == null) {
            return Collections.emptyList();
        }

        List<ReferenceModel> ret = references.stream()
                .filter(rm -> rm.matches(mainTable, otherTable))
                .collect(Collectors.toList());
        return ret;
    }

    public TableModel lookupTable(TableModel.Key tableKey) {
        TableModel ret = Utils.value(tablesNavigatable.ceilingEntry(tableKey));
        if (ret == null) {
            while ((tableKey = tableKey.navigateUp()) != null) {
                ret = Utils.value(tablesNavigatable.ceilingEntry(tableKey));
                if (ret != null) {
                    break;
                }
            }
        }
        if (ret != null && !tableKey.matches(ret.getKey())) {
            return null;
        }
        return ret;
    }

    public Collection<TableModel> getTables() {
        return tables.values();
    }
}
