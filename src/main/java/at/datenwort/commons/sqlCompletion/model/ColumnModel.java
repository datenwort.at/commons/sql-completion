/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.model;

import java.util.Locale;
import java.util.Objects;

public class ColumnModel {
    private final TableModel tableModel;
    private final String name;
    private final String documentation;

    public ColumnModel(TableModel tableModel, String name, String documentation) {
        this.tableModel = tableModel;
        this.name = name.toLowerCase(Locale.ROOT);
        this.documentation = documentation;
    }

    public TableModel getTableModel() {
        return tableModel;
    }

    public String getName() {
        return name;
    }

    public String getDocumentation() {
        return documentation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ColumnModel that = (ColumnModel) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
