/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.model;

import at.datenwort.commons.sqlCompletion.Tuple;
import at.datenwort.commons.sqlCompletion.Utils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DatabaseTableModelProvider {

    public DatabaseModel createModelFromDatabase(Connection connection) throws SQLException {
        SimpleDatabaseModel model = new SimpleDatabaseModel();

        final Map<TableModel.Key, TableModel> tables = new LinkedHashMap<>();

        DatabaseMetaData dmd = connection.getMetaData();
        try (ResultSet rs = dmd.getColumns(null, null, "%", "%")) {
            while (rs.next()) {
                String tableCatalog = rs.getString("TABLE_CAT");
                String tableSchema = rs.getString("TABLE_SCHEM");
                String tableName = rs.getString("TABLE_NAME");
                String columnName = rs.getString("COLUMN_NAME");
                String documentation = rs.getString("REMARKS");

                TableModel tableModel = new TableModel(tableCatalog, tableSchema, tableName, null);

                tables.computeIfAbsent(tableModel.getKey(), key -> tableModel)
                        .addColumn(columnName, documentation);
            }
        }

        model.setupTables(tables);

        final Map<String, ReferenceModel> references = new LinkedHashMap<>();

        for (TableModel tableModel : tables.values()) {
            try (ResultSet rs = dmd.getImportedKeys(tableModel.getCatalog(), tableModel.getSchema(), tableModel.getName())) {
                while (rs.next()) {
                    String name = Utils.lower(Utils.nvl(rs.getString("FK_NAME"), rs.getString("PK_NAME")));
                    if (name == null) {
                        continue;
                    }

                    TableModel.Key pkTableKey = new TableModel.Key(
                            rs.getString("PKTABLE_CAT"),
                            rs.getString("PKTABLE_SCHEM"),
                            rs.getString("PKTABLE_NAME"));

                    TableModel.Key fkTableKey = new TableModel.Key(
                            rs.getString("FKTABLE_CAT"),
                            rs.getString("FKTABLE_SCHEM"),
                            rs.getString("FKTABLE_NAME"));

                    TableModel pkTable = model.lookupTable(pkTableKey);
                    TableModel fkTable = model.lookupTable(fkTableKey);

                    if (pkTable != null && fkTable != null) {
                        ColumnModel pkColumn = pkTable.lookupColumn(rs.getString("PKCOLUMN_NAME"));
                        ColumnModel fkColumn = fkTable.lookupColumn(rs.getString("FKCOLUMN_NAME"));

                        if (pkColumn != null && fkColumn != null) {
                            references.computeIfAbsent(name, key -> new ReferenceModel(pkTable, fkTable)).add(pkColumn, fkColumn);
                        }
                    }
                }
            }
        }

        model.setupReferences(references.values());

        return model;
    }
}
