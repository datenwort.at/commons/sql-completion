/*
 * Copyright (c) 2021 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.sqlCompletion.model;

import at.datenwort.commons.sqlCompletion.Utils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("UnusedReturnValue")
public class TableModel {
    private final Key key;
    private final String catalog;
    private final String schema;
    private final String name;
    private final String documentation;
    private final Map<String, ColumnModel> columns = new LinkedHashMap<>();

    public static class Key implements Comparable<Key> {
        private final String catalog;
        private final String schema;
        private final String name;

        public Key(String catalog, String schema, String name) {
            this.catalog = Utils.lower(catalog);
            this.schema = Utils.lower(schema);
            this.name = Utils.lower(name);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return Objects.equals(catalog, key.catalog) && Objects.equals(schema, key.schema) && Objects.equals(name, key.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(catalog, schema, name);
        }

        public Key navigateUp() {
            if (catalog != null) {
                return new Key(null, schema, name);
            }
            if (schema != null) {
                return new Key(null, null, name);
            }

            return null;
        }

        @Override
        public int compareTo(Key other) {
            int ret = Utils.compare(name, other.name);
            if (ret != 0) {
                return ret;
            }
            ret = Utils.compare(schema, other.schema);
            if (ret != 0) {
                return ret;
            }
            ret = Utils.compare(catalog, other.catalog);
            if (ret != 0) {
                return ret;
            }
            return 0;
        }

        public boolean matches(TableModel.Key other) {
            if (catalog != null && !catalog.equals(other.catalog)) {
                return false;
            }
            if (schema != null && !schema.equals(other.schema)) {
                return false;
            }
            if (!name.equals(other.name)) {
                return false;
            }

            return true;
        }
    }

    public TableModel(String catalog, String schema, String name, String documentation) {
        this.key = new Key(catalog, schema, name);
        this.catalog = catalog;
        this.schema = schema;
        this.name = name;
        this.documentation = documentation;
    }

    public Key getKey() {
        return key;
    }

    public String getFullName() {
        return schema != null ? schema + "." + name : name;
    }

    public String getCatalog() {
        return catalog;
    }

    public String getSchema() {
        return schema;
    }

    public String getName() {
        return name;
    }

    public String getDocumentation() {
        return documentation;
    }

    public Collection<ColumnModel> getColumns() {
        return columns.values();
    }

    @Override
    public String toString() {
        return name;
    }

    public TableModel addColumn(String columnName, String documentation) {
        ColumnModel columnModel = new ColumnModel(this, columnName, documentation);
        columns.put(columnModel.getName(), columnModel);
        return this;
    }

    public ColumnModel lookupColumn(String name) {
        return columns.get(Utils.lower(name));
    }
}
