# SQL Completion

This library provides support in "sort-of" parsing an SQL statement and "code completion" for table and column names.

## Usage

```
SqlCompletionProvider sqlCompletionProvider = new SqlCompletionProvider(databaseMetadataBridge.getDatabaseModel(), sql);
sqlCompletionProvider.process();
```

afterwards, providing the char you can get a list of CompletionItems

```
List<CompletionItem> ret = sqlCompletionProvider.completeAt(offset)
```

## Known limitations

This is not a full-blown SQL parser. Its aim is not to correctly identify each and every token in an SQL statement, but to try to figure out the used tables/aliases/with statements as lenient as possible to being able to also work with "not-yet-finished"/erroneous SQL statements as you might have during writing a new statement.

Beside that:
* Actually no "context" is derived from the statemnt. So, having a UNION you would be able to reference a table included in another SELECT statement.
* Use of table alias is mandatory

## Changelog

# 0.7, 0.8
Deal with schema names if they are provided in the source sql for completion.
If a token is detected to be a schema name, the list of suggested tables is
filtered by that schema name and the insert text does not longer contain it to
avoid duplicate insertion of the schema text.

# 0.4
Add support for table reference suggestions in "join ... on" completion
